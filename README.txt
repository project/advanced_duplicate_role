INTRODUCTION
------------
Duplicate role permission module copy  an existing
role permission for modules,views,blocks,nodes and
taxonomies to the new roles.

INSTALLATION
------------
Extract the module in your sites/all/modules/ directory and enable.

AUTHORS
-------
Indivar

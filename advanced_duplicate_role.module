<?php
/**
 * @file
 * This module duplicates an existing role permissions to a new role.
 *
 * This includes all the permissions set for blocks, views and taxonomies.
 */

/**
 * Implements hook_help().
 */
function advanced_duplicate_role_help($path, $arg) {
  $output = '';
  switch ($path) {
    case "admin/people/advanced-duplicate-role-permission":
      $output = '<p>' . t("This module duplicate an existing role with the same permissions for modules, nodes, views,blocks and taxonomies as the original ones") . '</p>';
      break;
  }
  return $output;
}

/**
 * Implements hook_permission().
 */
function advanced_duplicate_role_permission() {
  return array(
    'administer advanced duplicate role permission' => array(
      'title' => t('Administer Advanced Duplicate Role Permission'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function advanced_duplicate_role_menu() {
  $items = array();
  $items['admin/people/advanced-duplicate-role'] = array(
    'title' => 'Duplicate Role Permission',
    'description' => 'A module that duplicates an existing role and its permissions to all views,blocks and content type.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('advanced_duplicate_role_form'),
    'access arguments' => array('administer advanced duplicate role permission'),
    'type' => MENU_LOCAL_TASK,
    'tab_parent' => 'admin/people/permissions',
  );
  return $items;
}

/**
 * Form submit. Insert records into database.
 */
function advanced_duplicate_role_form_submit($form, &$form_state) {
  $advanced_duplicate_role_base = $form_state['values']['advanced_duplicate_role_base'];
  $advanced_duplicate_role_new = $form_state['values']['advanced_duplicate_role_new'];

  if (db_table_exists('role')) {
    $role = new stdClass();
    $role->name = $advanced_duplicate_role_new;
    user_role_save($role);

    // Capture new role id.
    $new_role_rid = db_select('role', 'r')
      ->fields('r', array('rid'))
      ->condition('name', $advanced_duplicate_role_new, '=')
      ->execute()
      ->fetchAssoc();
    $role_id = $new_role_rid['rid'];
  }
  // Check Role exist and if yes then fetch all views,
  // menus and block permissions and assign to the new role.
  if (isset($advanced_duplicate_role_base)) {
    if (module_exists('views')) {
      $result = db_select('views_display', 'n')
        ->fields('n',
          array('vid',
            'id',
            'display_title',
            'display_plugin',
            'position',
            'display_options',
          )
        )
        ->orderBy('display_options')
        ->execute();

      foreach ($result as $row) {
        $vid = $row->vid;
        $block_id = $row->id;
        $display_title = $row->display_title;
        $display_plugin = $row->display_plugin;
        $position = $row->position;
        $strenc = $row->display_options;
        $arr = unserialize($strenc);
        $arr['access']['role'][$role_id] = $role_id;
        $newarray = serialize($arr);
        db_update('views_display')
          ->fields(array('vid' => $vid))
          ->fields(array('id' => $block_id))
          ->fields(array('display_title' => $display_title))
          ->fields(array('display_plugin' => $display_plugin))
          ->fields(array('position' => $position))
          ->fields(array('display_options' => $newarray))
          ->condition('display_options', $strenc, '=')
          ->execute();
      }
    }
    if (module_exists('menu_item_visibility')) {
      $result = db_select('menu_links_visibility_role', 'n')
        ->fields('n', array('mlid', 'rid'))
        ->condition('n.rid', $advanced_duplicate_role_base, '=')
        ->execute();
      foreach ($result as $row) {
        $menu_permission = $row->mlid;
        db_insert('menu_links_visibility_role')
          ->fields(array(
            'mlid' => $menu_permission,
            'rid' => $role_id,
          ))
          ->execute();
      }
    }
    $result_block = db_select('block_role', 'n')
      ->fields('n', array('module', 'delta', 'rid'))
      ->condition('n.rid', $advanced_duplicate_role_base, '=')
      ->execute();
    foreach ($result_block as $row1) {
      $mod = $row1->module;
      $del = $row1->delta;
      db_insert('block_role')
        ->fields(array(
          'module' => $mod,
          'delta' => $del,
          'rid' => $role_id,
        ))
        ->execute();
    }
  }
  // Permission for old role in table "permission".
  if (db_table_exists('role_permission')) {
    $old_perms = db_select('role_permission', 'rp')
      ->fields('rp', array('permission', 'module'))
      ->condition('rid', $advanced_duplicate_role_base, '=')
      ->execute();

    while ($data = $old_perms->fetchAssoc()) {
      // Insert permissions on new role.
      $new_role_perms = db_insert('role_permission');
      $new_role_perms->fields(
        array(
          'rid' => $new_role_rid,
          'permission' => $data['permission'],
          'module' => $data['module'],
        )
      );
      $new_role_perms->execute();
    }
  }

  drupal_set_message(t('New role %role_name added successfully.', array('%role_name' => $advanced_duplicate_role_new)));
  drupal_flush_all_caches();
}

/**
 * Form validate. Validate data before insert into database.
 */
function advanced_duplicate_role_form_validate($form, &$form_state) {
  $advanced_duplicate_role_new = $form_state['values']['advanced_duplicate_role_new'];
  $roles = user_roles();
  if (in_array($advanced_duplicate_role_new, $roles)) {
    form_set_error('advanced_duplicate_role_new', t('This role %role_name already exists. Please try a different name.', array('%role_name' => $advanced_duplicate_role_new)));
  }
}

/**
 * Module selection interface.
 */
function advanced_duplicate_role_form() {
  $form = array();
  $u_roles = user_roles();
  asort($u_roles);
  $options = array();
  $options[] = t('-- Please Select One Role --');
  foreach ($u_roles as $key => $value) {
    $options[$key] = $value;
  }

  $form['advanced_duplicate_role_base'] = array(
    '#type' => 'select',
    '#title' => t('Choose role to duplicate'),
    '#default_value' => variable_get('advanced_duplicate_role_base', 0),
    '#description' => t("Select role to duplicate"),
    '#options' => $options,
  );

  $form['advanced_duplicate_role_new'] = array(
    '#type' => 'textfield',
    '#title' => t('New role'),
    '#default_value' => variable_get('advanced_duplicate_role_new', ''),
    '#required' => TRUE,
    '#size' => 40,
    '#maxlength' => 40,
    '#description' => t("New role name"),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create new role'),
  );
  return $form;
}
